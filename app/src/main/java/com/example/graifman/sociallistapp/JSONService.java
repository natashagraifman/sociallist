package com.example.graifman.sociallistapp;

import android.test.suitebuilder.annotation.LargeTest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JSONService {
    @GET("s/50vmlj7dhfaibpj/sociais.json")
    Call<SocialList> getSocialList();
}
